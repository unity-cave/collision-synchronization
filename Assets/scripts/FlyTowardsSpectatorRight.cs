﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cave;

public class FlyTowardsSpectatorRight : CollisionSynchronization /*MonoBehaviour*/ {

	float speed = 4;
	float z = 10;
	float x = 2f;
	float t = 80;

	bool destroy = false;

	public FlyTowardsSpectatorRight()
		: base(new[] { Cave.EventType.OnCollisionEnter })
	{

	}

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {
		z = z - speed * TimeSynchronizer.deltaTime;
		if (!destroy) {
			transform.position = new Vector3 (2f, 0.0f, z);
		}

		if (destroy) {
			x = x + speed * TimeSynchronizer.deltaTime;
			transform.position = new Vector3(x, 0.0f, z);
		}
		if (t < -80) {
			Destroy (this.gameObject);
		}
		t--;
	}

	public override void OnSynchronizedCollisionEnter(GameObject other) {
		Debug.Log (other.name);
		if (other.name == "Lasersword") {
			//			x = x - speed * TimeSynchronizer.deltaTime;
			//			transform.position = new Vector3 (x, 0.0f, z);
			destroy = true;
			Debug.Log ("Rechts getroffen");
		}	
	}

	/*public void OnCollisionEnter(Collision other) {
		Debug.Log (other.gameObject.name);
		if (other.gameObject.name == "Lasersword") {
			//			x = x - speed * TimeSynchronizer.deltaTime;
			//			transform.position = new Vector3 (x, 0.0f, z);
			destroy = true;
			Debug.Log ("Blöööööööd Rechts");
		}	
	}*/
}
