﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using Cave;

public class SpawnObjects : MonoBehaviour {

	GameObject cubeLeft;
	GameObject cubeRight;

	float currTime = 0;

	// Use this for initialization
	void Start () {
		//InvokeRepeating ("Spawn",0f, 2f);
		cubeLeft = (GameObject) Resources.Load ("LeftSide");
		cubeRight = (GameObject) Resources.Load ("RightSide");
		GameObject obj = Instantiate(cubeRight);
		NetworkServer.Spawn(obj);

	}
	
	// Update is called once per frame
	void Update () {
		currTime += TimeSynchronizer.deltaTime;
				if (currTime >= 2f) {
					Spawn ();
					currTime = 0;
				}

	}

	void Spawn() {
		//Instantiate(Resources.Load("RightSide"), new Vector3(0.5f, 0.0f, 10f), Quaternion.identity);
		//Instantiate(Resources.Load("LeftSide"), new Vector3(-0.5f, 0.0f, 10f), Quaternion.identity);
		GameObject obj = Instantiate(cubeRight);
		NetworkServer.Spawn(obj);
	}
}
